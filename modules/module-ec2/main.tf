resource "aws_instance" "my_ec2" {
  ami                         = "ami-02b01316e6e3496d9"
  instance_type               = var.instance_type
  subnet_id                   = var.subnetid
  security_groups             = var.sgid
  key_name                    = "keyname"
  associate_public_ip_address = true

}

/*
resource "aws_key_pair" "my_keyname" {
  key_name   = "keyname"
  public_key = file("~/.ssh/id_rsa.pub")
}
*/
