output "vpcid" {
  value = aws_vpc.my_vpc.id
}

output "subnetid" {
  value = aws_subnet.my_subnet.id
}