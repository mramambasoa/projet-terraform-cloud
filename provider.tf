terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.38.0"
    }
  } 

  cloud {
                  organization = "dataaide"            #Name of the orgination in Terraform Cloud
     workspaces {
                  name         = "DataAide-Workspace"  #workspace you are sending to in cloud
     }
   }
   
  }



provider "aws" {
  region  = "eu-west-3"
  profile = "default"
}