module "module-vpc" {
  source = "./modules/module-vpc"

  cidr_block_vpc    = "10.0.0.0/16"
  cidr_block_subnet = "10.0.1.0/24"
}

module "module-ec2" {
  source = "./modules/module-ec2"

  instance_type = "t2.micro"
  subnetid      = module.module-vpc.subnetid
  sgid          = [module.module-sg.sgid]
}

module "module-sg" {
  source = "./modules/module-sg"

  vpcid = module.module-vpc.vpcid
}


