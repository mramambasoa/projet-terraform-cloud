/*
output "vpcid" {
  value = aws_vpc.my_vpc.id
}

output "subnetid" {
  value = aws_subnet.my_subnet.id
}
*/

output "ec2_external_ip" {
  #value = aws_instance.my_ec2.associate_public_ip_address
  value = module.module-ec2.ec2_external_ip
}