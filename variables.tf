variable "cidr_block_vpc" {
  type    = string
  default = "10.0.0.0/16"
}

variable "cidr_block_subnet" {
  type    = string
  default = "10.0.1.0/24"
}

variable "ami" {
  type    = string
  default = "ami-02b01316e6e3496d9"
}

